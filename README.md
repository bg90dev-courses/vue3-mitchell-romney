# VueJS 3 Course by The Earth is Square (Mitchell Romney)

![Project-Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![Status-Finished](https://img.shields.io/badge/Status-Finished-blue.svg)
![Maintained-No](https://img.shields.io/badge/Maintained-No-red.svg)
<a href="https://gitlab.com/bg90dev-courses/vue3-mitchell-romney" alt="GitLab Repository Link">
  <img alt="gitlab repo" src="https://img.shields.io/badge/gitlab-purple?logo=gitlab"/>
</a>
<a href="https://vuejs.org/" alt="Documentation Link">
  <img alt="Documentation Link" src="https://img.shields.io/badge/Made_with-VueJS-42b983"/>
</a>

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://borjag90dev-twitter-clone.herokuapp.com/)

Curso de Vue3 de The Earth is Square (Mitchell Romney) 
https://www.youtube.com/watch?v=ZqgiuPt5QZo

## Built With
### Technologies
<a href="https://vuejs.org/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vueJS.png" width=50 alt="VueJS"></a>
<a href="https://sass-lang.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/sass.png" width=50 alt="Sass"></a>
<a href="https://www.javascript.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript"></a>
<a href="https://nodejs.org/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/nodejs.png" width=50 alt="NodeJS"></a>

### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author
* **Borja Gete**

<a href="mailto:borjag90dev@gmail.com" alt="Borja Gete mail"><img src="https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail" title="Go To mail" alt="Borja Gete mail"/></a> <a href="https://gitlab.com/BorjaG90" alt="Borja Gete Gitlab"><img src="https://img.shields.io/badge/BorjaG90-purple?style=for-the-badge&logo=gitlab" title="Go To Github Profile" alt="Borja Gete Github"/></a> <a href="https://github.com/BorjaG90" alt="Borja Gete Github"><img src="https://img.shields.io/badge/BorjaG90-black?style=for-the-badge&logo=github" title="Go To Github Profile" alt="Borja Gete Github"/></a> <a href="https://linkedin.com/in/borjag90" alt="Borja Gete LinkedIn"><img src="https://img.shields.io/badge/BorjaG90-blue?style=for-the-badge&logo=linkedin" title="Go To LinkedIn Profile" alt="Borja Gete LinkedIn"/></a>
